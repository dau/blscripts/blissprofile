import getpass as _getpass
from io import StringIO as _StringIO
from contextlib import contextmanager as _contextmanager

import yappi as _yappi


@_contextmanager
def profile():
    # https://github.com/sumerc/yappi/blob/master/doc/api.md
    _yappi.set_context_backend("greenlet")
    _yappi.clear_stats()
    _yappi.set_clock_type("wall")
    _yappi.start(builtins=False)
    try:
        yield
    finally:
        _yappi.stop()
    ystats = _yappi.get_func_stats()
    _print_stats(ystats)
    _save_stats(ystats)


def _print_stats(ystats: _yappi.YFuncStat) -> None:
    # https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats
    sortby = "calls"
    sortby = "cumulative"  # including subcalls
    # sortby = "time"  # excluding subcalls
    nlinesmax = 50

    s = _StringIO()
    pstat = _yappi.convert2pstats(ystats)
    pstat.stream = s
    pstat = pstat.sort_stats(sortby)
    pstat.print_stats(nlinesmax)
    print(s.getvalue())


def _save_stats(ystats: _yappi.YFuncStat) -> None:
    user = _getpass.getuser()
    basename = f"/tmp_14_days/blissstats_{user}"

    filename = basename + "_callgrind.pyprof"
    ystats.save(filename, type="callgrind")
    print(f"Inspect: kcachegrind {filename}")

    filename = basename + "_pstat.pyprof"
    ystats.save(filename, type="pstat")
    print(f"Inspect: pyprof2calltree -k -i {filename}")

    print(f"Flame graph: flameprof {filename} > {filename}.svg ")
    print(f"Flame graph: snakeviz {filename}")
