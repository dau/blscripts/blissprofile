import time as _time
import getpass as _getpass
from tabulate import tabulate as _tabulate
import itertools as _itertools
import numpy as _numpy
from bliss import setup_globals as _setup_globals


_setup_globals.user_script_load(
    "/users/blissadm/local/xrpd/blissprofile/blissprofile.py"
)


def reset_streamline(func):
    def wapper(*arg, **kw):
        try:
            return func(*arg, **kw)
        finally:
            _setup_globals.streamline_scanner.dryrun = False
            _setup_globals.streamline_scanner.verify_qrcode = False
            _setup_globals.streamline_scanner.autotune_qrreader_per = "baguette"
            _setup_globals.streamline_scanner.optimize_exposure_per = "baguette"

    return wapper


@reset_streamline
def timeit_holder_scan(**requested):
    truefalse = True, False
    per = "", "sample", "baguette"

    _setup_globals.wa()

    headers = [
        "dryrun",
        "verify_qrcode",
        "autotune_qrreader_per",
        "optimize_exposure_per",
        "time (sec/sample)",
    ]
    data = list()

    with _setup_globals.bench():
        try:
            for (
                dryrun,
                verify_qrcode,
                autotune_qrreader_per,
                optimize_exposure_per,
            ) in _itertools.product(truefalse, truefalse, per, per):
                if not _is_requested(requested, "dryrun", dryrun):
                    continue
                if not _is_requested(requested, "verify_qrcode", verify_qrcode):
                    continue
                if not _is_requested(
                    requested, "autotune_qrreader_per", autotune_qrreader_per
                ):
                    continue
                if not _is_requested(
                    requested, "optimize_exposure_per", optimize_exposure_per
                ):
                    continue
                if not autotune_qrreader_per:
                    autotune_qrreader_per = None
                if not optimize_exposure_per:
                    optimize_exposure_per = None
                _setup_globals.streamline_scanner.dryrun = dryrun
                _setup_globals.streamline_scanner.verify_qrcode = verify_qrcode
                _setup_globals.streamline_scanner.autotune_qrreader_per = (
                    autotune_qrreader_per
                )
                _setup_globals.streamline_scanner.optimize_exposure_per = (
                    optimize_exposure_per
                )
                time_per_sample = _timeit_streamline_scanner_run()
                row = [
                    dryrun,
                    verify_qrcode,
                    autotune_qrreader_per,
                    optimize_exposure_per,
                    time_per_sample,
                ]
                row = [s if s is not None else "-" for s in row]
                data.append(row)
        finally:
            print()

            table = _tabulate(data, headers=headers)
            print(table)
            print()

            user = _getpass.getuser()
            filename = f"/tmp_14_days/blissstats_{user}_streamline.txt"
            with open(filename, "w") as f:
                f.write(table)

            print(f"Saved in '{filename}'")
            print()


@reset_streamline
def timeit_holder_scan_original(dryrun=False):
    timeit_holder_scan(
        dryrun=dryrun,
        verify_qrcode=True,
        autotune_qrreader_per="sample",
        optimize_exposure_per="sample",
    )


@reset_streamline
def timeit_holder_scan_new(dryrun=False):
    timeit_holder_scan(
        dryrun=dryrun,
        verify_qrcode=False,
        autotune_qrreader_per="baguette",
        optimize_exposure_per="baguette",
    )


@reset_streamline
def timeit_holder_scan_dry():
    timeit_holder_scan(
        dryrun=True,
        verify_qrcode=False,
        autotune_qrreader_per="",
        optimize_exposure_per="",
    )


@reset_streamline
def profile_holder_scan():
    _setup_globals.wa()

    _setup_globals.streamline_scanner.dryrun = False
    _setup_globals.streamline_scanner.verify_qrcode = False
    _setup_globals.streamline_scanner.autotune_qrreader_per = None
    _setup_globals.streamline_scanner.optimize_exposure_per = None
    with _setup_globals.bench():
        with _setup_globals.current_session.env_dict["user"].profile():
            _setup_globals.streamline_scanner.run(current_holder=True)


@reset_streamline
def timeit_optimize_exposure():
    _setup_globals.wa()

    t0 = _time.time()
    _setup_globals.streamline_scanner.determine_exposure_conditions()
    conditions1 = _setup_globals.streamline_scanner._exposure_conditions
    t1 = _time.time()
    _setup_globals.streamline_scanner.determine_exposure_conditions_individually()
    conditions2 = _setup_globals.streamline_scanner._exposure_conditions
    t2 = _time.time()

    print(f"Optimization time (ascan): {(t1-t0)/16:.3f} (sec/sample)")
    print(f"Optimization time (ct): {(t2-t1)/16:.3f} (sec/sample)")

    for cond1, cond2 in zip(conditions1, conditions2):
        if cond1 == cond2:
            print("Identical condition:", cond1)
        else:
            print("Different condition (ascan, ct):", cond1, cond2)


def test_xrpd_processor():
    _setup_globals.wa()
    for _ in range(10):
        _setup_globals.newdataset()
        _setup_globals.loopscan(100, 1)
        _setup_globals.loopscan(400, 1)
        _setup_globals.loopscan(100, 1)


def _is_requested(requested: dict, key: str, value: float) -> bool:
    expected = requested.get(key)
    if expected is None:
        return True
    return value == expected


def _timeit_streamline_scanner_run():
    times = list()
    for _ in range(3):
        t0 = _time.time()
        _setup_globals.streamline_scanner.run(current_holder=True)
        t1 = _time.time()
        times.append((t1 - t0) / 16)
    return f"{_numpy.mean(times):.3f} ± {_numpy.std(times):.3f}"
